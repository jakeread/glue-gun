## Glue Gun

A hotend / extruder for FDM machines. 

![gg](log/2021-01-24_hotend.mp4)
![gg](log/2020-12-06_gluegun.png)