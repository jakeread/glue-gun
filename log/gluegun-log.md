## Hotend Development Log 

## 2020 12 06

Did a bunch of development for this thing offline. 

![gg](2020-12-06_gluegun.png)

It's E3D hotend compatible, I'm using a Dragon hotend. Has a loadcell inline to measure extrusion force, hotend fan but no part cooling fan yet, etc. 

## 2020 12 10 

Assembled one of these, have some hardware bugs but will continue to bring this online instead of revisioning:

- could use a third thru-bolt to get the planar constraint w/ the second half of the hotend-pinching clamshell 
- loadcell to bowden connector does not clear (visible in CAD: move out or move up) 
- m4 head-thru on top of loadcell on hotend not wide enough 
- more pusher-part (extruder / top half) clearance to plate, notice misalignment / out of squareness at loadcell mount 

## 2020 12 27

Revisioning this to bring it up to speed with toolchanger deltas, also found a new loadcell to suit the smaller toolchanger design, that's UXCell PN `a15110200ux0247`. 

So I've to revision to tighten this up, but it should be pretty straightforward. I think I do this, then fab it all, and can bring loadcell probes in to the mix once I have two of these swapping pretty well... would love to have i.e. an 0.8mm infill tool and 0.4mm perimeter tool. 

- adjustment for smaller loadcell, 
- adjustment to hotend pinching clamshell
- rm interference between hotend / bowden top and loadcell 
- more-better planar constraint on hotend / coolside / front-of-hotend clamshell 

Great, this is done, so I'll fab and then bring heaters & loadcells online. 

![gg](2020-12-27_glue-gun.png)

## 2021 01 24

This part works, new toolchanger feels good also. 

![he](2021-01-24_hotend.mp4)

![squeeze](2021-01-24_hotend-squeeze.mp4)

![print](2021-01-24_hotend-print.mp4)

## 2021 02 03 

NEXT REV: you've mounted the hotend static, and the motor floating post-loadcell. However, loadcell deflection is really minimal, and you could fix the motor / mount the hotend on loadcells. This would let you touch-probe the bed w/ the nozzle itself, for zero calibration etc. 
## 2021 02 27

I have this together and, though some network struggles still exist, am able to capture data from the hotend. 

This was calibrated with 50g -> 1kg, in 50g increments, but I think there is some hysteresis as there is a PTFE tube in the filament load path... 

I've gathered [two](2021-02-26_extruderTestData_1.json) [datasets](2021-02-26_extruderTestData_2.json), running filament at 100, 200, 300, and 400 mm / min. The datasets are time-series and include extruder feedrate (in mm/sec), loadcell readings (in netwons) and temperature readings. The time steps / readings should be synced to about ~ 2ms. 

![res](2021-02-26_extruder-test.mp4)
![img](2021-02-26_extruderTestData_2.png)

## 2021 05 28

In terms of hotends, we have this loadcell mounted thing. I can likely do a smaller revision w/o the loadcell. Wants to open up for cleaner mount-up post, lighter overall.

OK it's looking pretty good already. I think I can optimize for spare space on the left hand side, and come up with a way to mount the HE fan and a part cooling fan... 

I'm going to give it a break, come back with fresh eyes. Needs two fans, I think I want a touch probe, but maybe best to continue to do this with the bed - and anyways there's room in front of the hotend for a probe if I want i.e. inductive or something later on. Heater mount... Probe can be tool-down and tap with the bottom of the toolchanger - that's way better, then an offset to the tip. 

- he cooling
- part cooling 
- heat circuit mount 
- set-down post 

Well, what I have now will do but it doesn't have a part cooling fan. There's some spare space to have a put-down arm, but not a tonne. 

- needs feed clamp 

Otherwise it's close, I'll close out tomorrow. 

- clamp 
- cleanup fillets... 
- dropoff post if you're ambitious ? left side, vertical hangup 
- part cooling shroud 
- move doc, set prints, return prints and fab-fxy


## 2021 05 29 

Just a few things left here - the awkward ends. I'm going to see about a face-on fan mount, do want a cooling fan and do want tool post layup zone. 

This schizm at v7, thx. 

OK, looking great with this modification. There is some spare space on the left here, for a set-down post. 

OK I have this wimpy post:

![post](2021-05-29_post-01.png)

But I think I can do much better, moving this to the left but using a similar topology. I know I'll eventually want a wiper at the nozzle / etc. 

This is really a tricky design problem, the post. It's maybe a bit easier if we give up on the 2d putdown, and allow that the tool can move in Z when putting down, then we could just rest it on tray, basically. 

But I think I'm done with this for now, will try the current post. 

Alright, calling it. 

## 2021 06 01 

I've built this up now, it's nice and crisp. Last note:

- the extruder gear preloader-spring is potentially in the whey of large-x motion... i.e. out on the right side, extends too far out of the tool boundary. lever arm could swing around / above centerline, and same post could be used to zip-tie PTFE feeder input. 

## 2021 09 01 

I'm back to do this again w/ a new toolchanger design. I also have wanted to see about improving some of the data by shifting the relationship between the loadcell and the hotend itself... i.e. floating hotend, motor static mounted. I should also want to have a mount up top for a potential future w/ filament width sensor inline... which I have a sneaking suspicion is a key element for really good prints. Then a tool set-down post that works well. 

I'm going to rough out a loadcell-to-hotend relationship, and I'd like to see that setup such that I can swap in whatever V6 hotend is available. 

OK I have the gear tensioning thing setup, I am going to omit one of the motor screws, but I think I should be a little more careful here: I am going to add an alu spacer so that I can tighten the hinge (thing rotates around one of the motor mount screws) without pinching the hinge points. 

This is coming along much cleaner than other versions:

![prog](2021-09-01_progress.png)

Moving on to a little hotend fan clip, I can't find a 30mm square fan at 24v, so I need to use the 40mm. I suppose it makes the most sense to do this mounted up against the piece that pinches the hotend up top. 

I also have the circuit mounting, and put-down post issue. I think circuits lay on the lefthand plane... then I would additionally want a cooling fan, although maybe I just redirect the hotend cooling fan into the part. 

I'm feeling stuck again with the hang-up post, for which I want some kind of gripping flexure with a little bit of bi-stability, but can't quite figure what the most sensible way is. 

Here's what I came up with, 

![certified](2021-09-01_post-forklift.png) 

I like these kind of 'sideways forklift' situations, IDK how well this tiny flexure will do. Reaches behind the CG of the tool, at least that's the idea, then most of that beam is 'deep-ish' to handle the load. I should fit it with some ziptie holes, I'll run the bowden tube up the top of this thing, probably, right? 

Ah damn I've ignored a pretty glaring issue here, the loadcell amp is mounted right above where one of the loadcell M5s connects to the chassis. Thought I was done! 

... so, I could print these circuit mounts separately and mate them, but we all know they're not going to go on with any perpendicularity, which would mess up the parked positioning. 

I'm going to do the lazy thing and just get away with this blaring thru hole, winning back some strength that I should probably have anyways by bringing the side of the circuit mount into connection with the motor boss. 

Alright, attrocity committed:

![bad](2021-09-01_hotfix.png)

OK, it's 10pm and I'm spent but I think this is done. 

![done](2021-09-01_gg-newagain.png) 

I have to order that alu spacer, but otherwise I'm ready to print and test this. Will probably get the -fxy together and moving again before I fab this... back here in a week or less, let's hope. 

## 2021 09 22 

For NIST, I want to print little tiny things - so we want a part cooling fan (PCF). This is actually a totally not-ignorable aspect of hotend design, turns out.

My first shot obstructed the hotend cooling fan (HCF)... this was it:

![pcf](2021-09-22_first-pcf.png) 

So I think I will try to move the HCF up, to get the PCF below it; both should have unobstructed flow into the fan. 

Well I can just barely make this work all flush-like:

![pcf](2021-09-22_pcf-almost.png)

Though to really do it, I need about 5mm more stack on Z, I figure I move the nozzle down by about as much, or the motor up, but either (done well) has me re-printing the main plate, which is an overnight job. 
